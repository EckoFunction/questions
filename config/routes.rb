Rails.application.routes.draw do
  devise_for :users
  root 'index#home'

  get 'question/:id', to: 'questions#show', as: :question_show
  get 'questions', to: 'questions#index', as: :questions_list

  authenticate :user do
    get 'ask_the_question', to: 'questions#new'
    post 'ask_the_question', to: 'questions#create'
    get 'questions/my', to: 'questions#user_questions', as: :my_questions
    get 'answers/my', to: 'answers#user_answers', as: :my_answers
    post 'new_answer', to: 'answers#new', as: :new_answer
    post 'create_answer', to: 'answers#create', as: :create_answer
    post 'new_correction', to: 'corrections#new', as: :new_correction
    post 'create_correction', to: 'corrections#create', as: :create_correction
    post 'new_accept', to: 'corrections#new_accept', as: :new_accept
    post 'create_accept', to: 'corrections#create_accept', as: :create_accept
  end
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
