puts 'start seeding user'

users_email = 'test@test.ru'
users_password = 'qweqwe'
user = User.first_or_create email: users_email, password: users_password, password_confirmation: users_password 

puts "user import was succesfuly, with email: #{users_email} and password #{users_password}"

puts "start seeding the questions for #{users_email} user" 

for i in 1..10 do 
	Question.find_or_create_by text: "Тестовый вопрос №#{i}", user: user
end

puts 'questions import was succesfuly'