class CreateAnswers < ActiveRecord::Migration
  def change
    create_table :answers do |t|
    	t.string :text
    	t.belongs_to :users
    	t.belongs_to :questions

      t.timestamps null: false
    end
  end
end
