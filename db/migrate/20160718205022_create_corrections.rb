class CreateCorrections < ActiveRecord::Migration
  def change
    create_table :corrections do |t|
    	t.string :text
    	t.boolean :accepted
    	t.belongs_to :user
    	t.belongs_to :answer

      t.timestamps null: false
    end
  end
end
