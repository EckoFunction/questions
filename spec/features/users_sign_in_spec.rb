require 'rails_helper'

feature "User trying to sign in" do

	let(:user) { create :user }	
	before { visit new_user_session_path }


	scenario "user types correct data" do
		expect(page).to have_content( t('headers.log_in') )

		within "#new_user" do
			fill_in "user_email", with: user.email
			fill_in "user_password", with: user.password
		end

		click_button t('labels.forms.log_in')

		expect(page).to have_content( t('labels.header.greeting', user_name: user.email) )
	end

	scenario "user types wrong email" do

		user_email = "some@email.ru"
		user_password = "qweqwe"

		within "#new_user" do
			fill_in "user_email", with: user_email
			fill_in "user_password", with: user_password
		end

		click_button t('labels.forms.log_in')

		expect(page).to have_content( t('devise.failure.user.not_found_in_database') )
	end

	scenario "user types wrong password" do

		# toDO: can't write this test, cuz sign_in form says that user is not foud, but in Web version it's all right

		# visit new_user_registration_path

		# expect(page).to have_content( t 'labels.header.log_in')

		# user_email = "test@test.ru"
		# user_password = "password"

		# within "#new_user" do
		# 	fill_in "user_email", with: user_email
		# 	fill_in "user_password", with: user_password
		# 	fill_in "user_password_confirmation", with: user_password
		# end

		# click_button t('labels.forms.sgin_up')
		# click_link t('labels.header.log_out')

		# visit new_user_session_path

		# within "#new_user" do
		# 	fill_in "user_email", with: user_email
		# 	fill_in "user_password", with: user_password.split("").shuffle.join
		# end

		# click_button t('labels.forms.log_in')

		# expect(page).to have_content( t('activerecord.errors.models.user.attributes.password_confirmation.confirmation') )
	end
end