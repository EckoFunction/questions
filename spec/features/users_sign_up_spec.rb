require 'rails_helper'

feature "User trying to sign up" do

	let!(:user) { create :user }
	before { visit new_user_registration_path }

	scenario 'user fill wrong email and empty password' do
		user_email = "wrong_email"
		user_password = ""

		within "#new_user" do
			fill_in "user_email", with: user_email
			fill_in "user_password", with: user_password
			fill_in "user_password_confirmation", with: user_password
		end

		click_button t('labels.forms.sgin_up')

		expect(page).to have_content( t('activerecord.errors.models.user.attributes.email.invalid') )
		expect(page).to have_content( t('activerecord.errors.models.user.attributes.password.blank') )
	end

	scenario 'user fill wrong password confirmation' do
		user_email = "some@email.ru"
		user_password = "qweqwe"

		within "#new_user" do
			fill_in "user_email", with: user_email
			fill_in "user_password", with: user_password
			fill_in "user_password_confirmation", with: user_password + "qq"
		end

		click_button t('labels.forms.sgin_up')

		expect(page).to have_content( t('activerecord.errors.models.user.attributes.password_confirmation.confirmation') )
	end

	scenario 'user fill too short password' do
		user_email = "some@eamil.ru"
		user_password = "qwe"

		within "#new_user" do
			fill_in "user_email", with: user_email
			fill_in "user_password", with: user_password
			fill_in "user_password_confirmation", with: user_password
		end

		click_button t('labels.forms.sgin_up')

		expect(page).to have_content( t('activerecord.errors.models.user.attributes.password.too_short') )
	end	

	scenario 'user trying to register in alerady exists email' do

		within "#new_user" do
			fill_in "user_email", with: user.email
			fill_in "user_password", with: user.password
			fill_in "user_password_confirmation", with: user.password
		end

		click_button t('labels.forms.sgin_up')

		expect(page).to have_content( t('activerecord.errors.models.user.attributes.email.taken') )
	end	

	scenario 'create new user should be successful' do
		expect(page).to have_content( t 'labels.header.log_in')

		user_email = "test@test.ru"
		user_password = "password"

		within "#new_user" do
			fill_in "user_email", with: user_email
			fill_in "user_password", with: user_password
			fill_in "user_password_confirmation", with: user_password
		end

		click_button t('labels.forms.sgin_up')

		expect(page).to have_content( t('labels.header.greeting', user_name: user_email) )
		# validation errors
		expect(page).not_to have_content( t('activerecord.errors.models.user.attributes.email.invalid') )
		expect(page).not_to have_content( t('activerecord.errors.models.user.attributes.password.blank') )
		expect(page).not_to have_content( t('activerecord.errors.models.user.attributes.password_confirmation.confirmation') )
		expect(page).not_to have_content( t('activerecord.errors.models.user.attributes.password.too_short') )

	end

end