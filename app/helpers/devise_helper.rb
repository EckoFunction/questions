module DeviseHelper
  def devise_error_messages!
    if  flash.empty?
      messages = resource.errors.map {|key,err| content_tag(:li, err, class: "alert") }.join
    elsif !flash.empty?
      messages = flash.map {|key,err| content_tag(:li, err, class: key) }.join
    else 
      return ''
    end

    if !messages.empty?
      html = <<-HTML
        #{messages}
      </div>
      HTML

      html.html_safe
    else
      return ''
    end
  end
end