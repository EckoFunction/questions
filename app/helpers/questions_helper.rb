module QuestionsHelper
	def redner_errors(object)
    if  object.errors.any?
      messages = object.errors.map {|key,err| content_tag(:li, err, class: "alert") }.join
    else 
      return ''
    end

    if !messages.empty?
      html = <<-HTML
      <div class="info"> 
        #{messages}
      </div>
      HTML

      html.html_safe
    else
      return ''
    end
	end

end
