class AnswersController < ApplicationController

	def user_answers
		@answers = Answer.all.where(user: current_user).paginate(page: params[:page])
	end

	def new
		@answer = Answer.new(params.permit(:question_id))
		respond_to do |format|
			format.js
		end
	end

	def create
		@answer = Answer.new(answer_params)
		@answer.save

		redirect_to question_show_path(id: @answer.question.id) 
	end

	private

		def answer_params
			params.require(:answer).permit(
				:user_id,
				:question_id,
				:text
			)
		end

end
