class CorrectionsController < ApplicationController

	def new
		@correction = Correction.new(params.permit(:answer_id))
	end

	def create
		@correction = Correction.new(correction_params)
		@correction.save

		redirect_to question_show_path(id: @correction.answer.question.id)
	end

	def new_accept
		@correction = Correction.find(params[:correction_id])
		@correction.answer.text = Answer.find(params[:answer_id]).text + " " + @correction.text

		respond_to do |format|
			format.js
		end
	end

	def create_accept
		correction = Correction.find(params[:correction][:id])
		correction.accepted = true
		answer = correction.answer
		answer.text = params[:correction][:answer_attributes][:text]
		answer.save
		correction.save

		redirect_to question_show_path(id: correction.answer.question.id)
	end

	private

		def correction_params
			params.require(:correction).permit(
				:user_id,
				:answer_id,
				:text
			)
		end

end
