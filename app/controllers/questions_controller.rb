class QuestionsController < ApplicationController

	def index
		@questions = Question.paginate(page: params[:page])
	end

	def user_questions
		@questions = Question.where(user: current_user).paginate(page: params[:page])
		render 'index'
	end

	def new
		@question = Question.new
	end

	def create
		@question = Question.new(question_params)
		if @question.save
			# toDo: chenge the route after adding show action
			redirect_to question_show_path(@question)
		else
			render action: :new
		end
	end

	def show
		@question = Question.find(params[:id])
		if @question.nil?
			# add the error text
		end
	end

	private

		def question_params
			params.require(:question).permit(
				:user_id,
				:title,
				:text
			)		
		end
end
