# == Schema Information
#
# Table name: corrections
#
#  id         :integer          not null, primary key
#  text       :string
#  accepted   :boolean
#  user_id    :integer
#  answer_id  :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Correction < ActiveRecord::Base

	belongs_to :user
	belongs_to :answer

	validates :user, presence: true
	validates :answer, presence: true
	validates :text, presence: true

	accepts_nested_attributes_for :answer
end
